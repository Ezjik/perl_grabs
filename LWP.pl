use strict;
use warnings;
use XML::Simple;
use Data::Dumper;
#use Data::Dump qw(dump dd ddx);
use utf8;
use LWP::UserAgent;
use v5.14;
binmode(STDOUT,':utf8');
use Benchmark;

my $rMR = Benchmark->new;
my $tst;
my $sleep_on_third_request; # До тех пор пока не будет метода execute


## TODO LIST
# - Использовать токен при ошибки доступа
# - Обернуть все методы в execute
# - рефакторинг кода
# - Сделать Web морду
# - Придумать способ хранения данных и сбор статистики
# + Добавить возможность использовать токен
# + Дописать функцию сбора ссылок с профиля пользователя
# + Проврить работу на странице пользователя
# + Пределать сортировку -- Сделать древовидный выхлоп
# + Избавиться от дубликатов сообщений
# + Избавиться от глобальных переменных %messages, %posts %profiles
# + Доплнить вывод сообщений доп. информацией
# + Исправить ошибок связанные пустыми полями пример / БЫЛИ УДАЛЕННЫЕ СООБЩЕНИЯ

# Читаем конфиг
open(CONFIG, "<:utf8", "./config.cfg") or die "Can't open this file $!\n";
my $file=<CONFIG>;
my %config;
while (<CONFIG>) {
    chomp;                  # no newline
    s/#.*//;                # no comments
    s/^\s+//;               # no leading white
    s/\s+$//;               # no trailing white
    next unless length;     # anything left?
    my ($var, $value) = split(/\s*=\s*/, $_, 2);
    $config{$var} = $value;
} 


## Переменные для отправки запроса
my $ua = LWP::UserAgent->new;
my $SITE='https://api.vk.com/method/';
my $req; 
my $res;

my @threads; # Массив содержащий ссылки на хеши 


my $ID=$config{USER_ID};
my $OWNER_ID=$config{OWNER_ID}; # Группа по умолчанию
my $POST_ID=$config{POST_ID};
my $token="access_token=$config{access_token}";


# EXAMPLE FOR URLs
# FOR GROUP - https://api.vk.com/method/wall.get.xml?\
#owner_id=-23110002&count=100&offset=1&extended=1&v=5.42
# FOR COMMENTS - https://api.vk.com/method/wall.getComments.xml?\
#owner_id=-23110002&post_id=520491&need_likes=1&count=100&offset=0&extended=1&v=5.42
# FOR USERS

sub Messages_from_POST {
my %args=(  OWNER_ID    => $OWNER_ID,
            OFFSET      => 0,
            EXTENDED    => 1,
            NEED_LIKES  => 1,
            METHOD      => "wall.getComments.xml",
            @_,
);
$args{OFFSET}=$args{COUNT}*$args{FACTOR}+$args{OFFSET};
# print "$args{OFFSET}","\n";
my $offset="&offset=$args{OFFSET}";
my $count="&count=$args{COUNT}";
my $METHOD="$args{METHOD}";
my $Q="?";
my $MetPar;


given ($METHOD)
{
    when (/groups.get.xml/i) {
      $MetPar="user_id=$args{OWNER_ID}&extended=$args{EXTENDED}&v=5.42&$token$offset$count";
    }
    when (/wall.get.xml/i) {
      $MetPar="owner_id=$args{OWNER_ID}&need_likes=$args{NEED_LIKES}${count}${offset}&extended=$args{EXTENDED}&v=5.42";
      $MetPar="${MetPar}&${token}" if $args{OWNER_ID} !~ m/^-/;
    }
    when (/wall.getComments.xml/i) {
      $MetPar="owner_id=$args{OWNER_ID}&post_id=$args{POST_ID}&need_likes=$args{NEED_LIKES}${count}${offset}&extended=$args{EXTENDED}&v=5.42";
    }
    # when () { }
    # when () { }
}


  my $URL="${SITE}${METHOD}${Q}${MetPar}";
    # print "$URL\n";
    return $URL;
}



# <text>[id278240523|Наталья], а гадости предложил это пешком назад идти ? )))</text>
 sub check{                                              # Check the outcome of the response
  if ($res->is_success) {  print $res->content;}
  else { print $res->status_line, "\n"; }
}


sub Make_requests {
  my $count;
  my $N=0;
  my $URL=Messages_from_POST(@_, FACTOR => $N);
  my %options=@_;
  my $files="ID$options{OWNER_ID}_$options{METHOD}_";
  # print "$files${N}.xml \n";
  # print "$URL \n";
  if ( $sleep_on_third_request++ > 0 ){ 
    sleep 1;
    $sleep_on_third_request=0;  
  }
  $req = HTTP::Request->new(GET => $URL);
  $res = $ua->request($req, "./$files${N}.xml");
  $threads[0] = XMLin("./$files${N}.xml", KeyAttr  => "id");
  if ( exists $options{COUNT_MAX}) { $count=$options{COUNT_MAX} }
  else                             {$count=$threads[0]->{count} }

  while ( (++$N)*100 < $count ) {
   $URL=Messages_from_POST(@_, FACTOR => $N);
   $req = HTTP::Request->new(GET => $URL);
   $res = $ua->request($req, "$files${N}.xml");
   $threads[$N] = XMLin("./$files${N}.xml");
   if ( $sleep_on_third_request++ > 0 ){ 
    sleep 1;
    $sleep_on_third_request=0;  
    }
   # sleep 1;
  }
  # print Dumper(@threads);
}


sub concatenation {
  my %args=(  ref_to_hash => 0, 
              @_,
    );
  my ($tmpArr) =$args{ref_to_hash};
  my ($tmaProf)=$args{ref_to_prof} if(exists $args{ref_to_prof});

# print Dumper(@{$tmpArr});
  # print Dumper(@threads);

for  ( @threads ) {
given ($_) {
  # print "$_->{items}->{group}"," \n";
  # Объединение комментариев
    when (exists $_->{items}->{comment}) {
        foreach my $part (\%{$_->{items}->{comment}}) {
          # print $_;
            while (my ($k, $v) = each %{$part}) {
                if (! exists $tmpArr->{$k}) { $tmpArr->{$k}=$v } }}
  # Объединение профилей             
        foreach my $part (\%{$_->{profiles}->{user}}) {
          # print $_;
            while (my ($k, $v) = each %{$part}) {
                if (! exists $tmaProf->{$k}) { $tmaProf->{$k}=$v  } }}
    }
  # Объединение постов
    when (exists $_->{items}->{post}) {
        foreach my $part (\%{$_->{items}->{post}}) {
            while (my ($k, $v) = each %{$part}) {
                if (! exists $tmpArr->{$k}) { $tmpArr->{$k}=$v } }} 
    }
  # Объединение групп
    when (exists $_->{items}->{group}) {
        foreach my $part (\%{$_->{items}->{group}}) {
            while (my ($k, $v) = each %{$part}) {
                if (! exists $tmpArr->{$k}) { $tmpArr->{"-$k"}=$v } }}       
    }
}
}

}


sub count_double_value {
  my ($hash)=@_;
  my %stats;
  for my $part (keys %{$hash}) { map {$stats{$_}++} $hash->{$part}->{from_id} }
  for my $i (keys %stats) {print "$i = $stats{$i}\n"} 
}


sub search_message_from_id {
#  Интересуют 3 три тип сообщений
# 1 -- Сообщения самого пользователя
# 2 -- Ответы на сообщения пользователя
# 3 -- Сообщения на которые ответил пользователь

my %args=( @_ );
my ($UtoM)=$args{FOR_USERS_MESSAGES} if (exists $args{FOR_USERS_MESSAGES}) or 
                                     die "Не переданна переменная FOR_USERS_MESSAGES\n";
my ($ALL_messs)=$args{FULL_HASH}     if (exists $args{FULL_HASH}) or
                                     die "Не переданна переменная FULL_HASH\n";

    for  my $key (keys %{$ALL_messs}) {
        given ($ALL_messs->{$key})
        {
            when ($_->{from_id} == $ID) { $UtoM->{$key} =$_ }
            when ((exists $_->{reply_to_user})&& $_->{reply_to_user} == $ID) { $UtoM->{$key} =$_}
        }
    }
}

sub counts_el{
my %tmpHASH=@_;
my $count=0;
foreach my $part (sort ( keys %tmpHASH)) {
    $count++; 
    print "$part\n"
}
print "$count\n"
}

sub comment_others {

my %args=( @_ );
my ($key)=$args{KEY}                   if (exists $args{KEY}) or 
                                       die "Не переданна переменная KEY\n";
my ($ALL_messs)=$args{FULL_HASH}       if (exists $args{FULL_HASH}) or
                                       die "Не переданна переменная FULL_HASH\n";

my $count;
my @tmpArr;
$tmpArr[$count++]=$key;
    do {
      if(exists $ALL_messs->{$key}->{reply_to_comment}) { 
        $tmpArr[$count++]=$ALL_messs->{$key}->{reply_to_comment} }
      $key=$ALL_messs->{$key}->{reply_to_comment};
    } while (my $part=$ALL_messs->{$key}->{reply_to_comment});
    return @tmpArr;
}


sub sort_messages{
my %args=( @_ );
my ($UtoM)=$args{FOR_USERS_MESSAGES} if (exists $args{FOR_USERS_MESSAGES}) or 
                                     die "Не переданна переменная FOR_USERS_MESSAGES\n";
my ($ALL_messs)=$args{FULL_HASH}     if (exists $args{FULL_HASH}) or
                                     die "Не переданна переменная FULL_HASH\n";
my ($profiles)=$args{FULL_PROFILES}  if (exists $args{FULL_PROFILES}) or
                                     die "Не переданна переменная FULL_PROFILES\n";
my (%CP_H)=%{$args{FULL_HASH}}       if (exists $args{FULL_HASH});

my @M_Threads;
my (@sum_value, @tmpArr);
my $ArrInd=0;

# print Dumper($UtoM);

foreach my $part (sort {$b cmp $a} (keys  %{$UtoM}) ) {
my $count=0;
  if (not $part ~~ @sum_value) {
      # print "\nЦепочка - ",$part, ": ";
        if (exists $UtoM->{$part}->{reply_to_comment}) {
      # print "$part -- Сообщения не является одиночным -- $UtoM->{$part}->{text}\n";
      (@tmpArr)=comment_others(KEY        => $part,
                               FULL_HASH  => \%CP_H,
    );
      push @sum_value, @tmpArr;
      push @sum_value, $part; 
      for (reverse @tmpArr) { 
        delete($CP_H{"$_"}); 
        # print "$_ \n";`
        $M_Threads[$ArrInd][$count++]=$_;
      }
      $ArrInd++;
      @tmpArr=();
    }
    else {
      # print "$part -- Сообщения является одиночным    -- $UtoM->{$part}->{text}\n";
      my $tmpID=$ALL_messs->{$part}->{from_id};
      delete($CP_H{"$part"});
      $M_Threads[$ArrInd++][$count]=$part; 
    }
  }
  # else { print "Данный элемент уже проверялся $part \n" }
}

@M_Threads = sort { $a->[0] <=> $b->[0]} @M_Threads; # Делаем сортировка по первому столбцу
# print Dumper(@M_Threads);
return @M_Threads;
}



sub print_threads{
my %args=(@_);
my ($ALL_messs)=$args{FULL_HASH}     if (exists $args{FULL_HASH}) or
                                     die "Не переданна переменная FULL_HASH\n";
my (@SortMess)=@{$args{SORT_MESS}}  if (exists $args{SORT_MESS}) or
                                     die "Не переданна переменная SORT_MESS\n"; 
my ($profiles)=$args{FULL_PROFILES}  if (exists $args{FULL_PROFILES}) or
                                     die "Не переданна переменная FULL_PROFILES\n";
my %position;
my $countP=0;
my $otstup;
 # print Dumper(@SortMess);
my $count_mess=1;
  for my $i (0 .. $#SortMess) {
    print "\n";
    $countP=0;
    PRINT_OUTPUT: for my $j (0 .. $#{$SortMess[$i]}) {
      if (! exists $position{$SortMess[$i][$j]}) {
        # print $SortMess[$i][$j], "\n";
        my $items=$SortMess[$i][$j];
# ВРЕМЕННОЕ РЕШЕНИЕ: Если сообщение было удалено, то за место сообщения печатаем DELETED
        my $ID_user;
        my $FIO;
        my $text;
        if (exists $ALL_messs->{$items}->{text} ) {
          $text=$ALL_messs->{$items}->{text};
          $text=~ s/\s+/ /g;
          # print "$items\n";
          # ФИО юзверя
          $ID_user=$ALL_messs->{$items}->{from_id};
          $FIO="$profiles->{$ID_user}->{first_name} $profiles->{$ID_user}->{last_name}:";
        }
        else {
          $text="DELETED";
          $FIO="DELETED";
        }
        

        # ФИО отправителя на чей коммент отвечаем
        my $reply_to_comment;
        my $ID_repl;
        my $FIO_repl;
        my $reply_to_user;
        my $outputMess="$FIO";
        if (exists $ALL_messs->{$items}->{reply_to_user}) {
          $reply_to_comment="ответ на комментарий: $ALL_messs->{$items}->{reply_to_comment}";
          $ID_repl=$ALL_messs->{$items}->{reply_to_user};
          $FIO_repl="$profiles->{$ID_repl}->{first_name} $profiles->{$ID_repl}->{last_name}" ;
          $reply_to_user="Ответ пользователю: $FIO_repl" ;
          $outputMess="$FIO $reply_to_user";
        }
        
        print "\t"x$countP, "$outputMess -> $count_mess\n";
        print "\t"x$countP, "$text\n";
        $position{$items}=$countP;
        # $position{$items}=$countP++;
        $count_mess++;
      }
    }
      continue { 
        $countP++;
      }
  }

}



###### MAIN ######

# Собираем данные о пользователе
sub giveProfile{
  my %usrGroups;
  # Данные о группах
  Make_requests(METHOD    => "groups.get.xml",
                COUNT_MAX => "10",
                COUNT     => "10",
                OWNER_ID  => $ID
  );
  concatenation(ref_to_hash => \%usrGroups);
  counts_el(%usrGroups);
  givePosts(USR_GROUPS => \%usrGroups);
  print "Профиль проверен";
}

#https://vk.com/motopokatyn?w=wall-75561673_381449

# Собираем посты из стены группы\пользователя
sub givePosts{ 
  my %args=@_;
  my $usrGroups=$args{USR_GROUPS};


  foreach my $part (sort {$a cmp $b} (keys  %{$usrGroups})) { 
  my %posts; # Общий список тем в группе
  print $part, "\n";
    Make_requests(METHOD    => "wall.get.xml",
                  COUNT_MAX => "40", # Если не указано скачиваем всю стену
                  OWNER_ID  => $part,
                  COUNT     => "40",
                  OFFSET    => "1"
    );
    concatenation(ref_to_hash => \%posts);


  my $scr_name=$usrGroups->{$part}->{screen_name};
  my $spec_symb="?w=wall";
  my $part_1_URL="$scr_name$spec_symb$part";
  # print "$part_1_URL", "\n";
  # my $URL="http://vk.com/$scr_name$spec_symb 75561673_381449";
    giveMessages(POSTS_LISTS =>\%posts, OWNER_ID => $part, PART_1_URL => $part_1_URL );
    print "Группа проверена: $scr_name";
  }
}

sub giveMessages{
  my %args=@_;
  my $posts=$args{POSTS_LISTS};
  my $OWNER_ID=$args{OWNER_ID};
  my $part_1_URL=$args{PART_1_URL};
  my $URL="http://vk.com/$part_1_URL";


  foreach my $part (sort {$b cmp $a} (keys  %{$posts})) { 
    if ( $posts->{$part}->{comments}->{count} > 0) 
    {
      my %messages=(); # обший список сообщений
      my %profiles=(); # Список пользователей учавствующих в диалоге  
      # print "Проверка поста:\n$posts->{$part}->{text}\n";
      # Ищем сообщения от нашего пользователя
      Make_requests(METHOD      => "wall.getComments.xml", 
                    OWNER_ID    => $OWNER_ID,
                    COUNT_MAX   => "100",
                    COUNT       => "100",
                    # POST_ID     => $POST_ID
                    POST_ID     => $part
      );
      concatenation(ref_to_hash => \%messages,
                    ref_to_prof => \%profiles
      );
      if(exists $profiles{$ID}) {
        print "Пост: URL: ${URL}_${part} \n$posts->{$part}->{text}\n";
        my %UtoM=(); # Сообщения от юзверя другим пользователям
        print "Сообщения от пользователя $profiles{$ID}->{first_name}";
        # print " $profiles{$ID}->{last_name} есть в данном топике\n";
        search_message_from_id(FOR_USERS_MESSAGES => \%UtoM,
                               FULL_HASH          => \%messages
      );
        # counts_el(%UtoM);
        my @SortMess=sort_messages(FOR_USERS_MESSAGES  => \%UtoM,
                      FULL_HASH           => \%messages,
                      FULL_PROFILES       => \%profiles
      );
        # print Dumper(@SortMess);
        print_threads(SORT_MESS     => \@SortMess,
                      FULL_HASH     => \%messages,
                      FULL_PROFILES => \%profiles

      );
        print "\n"x5;
      }
      # else {print  "В данной теме постов нашего пользователя нет.", "\n"x5 }
    }
  }
}


giveProfile();

#_____________TESTS_____________#

# -------- Проверка можно ли писать в группе\юзверя
# check_of_rights





__END__

vk spy 

Входные данные
id_users


Что хочу получить
1) Следить за обновлениями на странице
1.1) Новые записи
1.1.1) Кто ставит лайки записи
1.1.2) Кто делает репост
1.1.3) Кто оставляет комментарии

1.2) Новые картинки
1.2.1) Кто ставит лайки записи
1.2.2) Кто делает репост
1.2.3) Кто оставляет комментарии

1.3) Новые видео
1.3.1) Кто ставит лайки записи
1.3.2) Кто делает репост
1.3.3) Кто оставляет комментарии

1.4) Новые заметки
1.4.1) Кто ставит лайки записи
1.4.2) Кто делает репост
1.4.3) Кто оставляет комментарии

1.5) Изминения в друзьях
1.5.1) Новых друзей
1.5.2) Удаленных друзей

1.6) Изминения в подписчиках
1.6.1) Новых подписчиков
1.6.2) Удаленных подписчиков

1.7) Изменения в сообществах и итересах(далее группы)
1.7.1) Вышла из группы
1.7.2) Вступила в группу

Сообщества и группы
2) Внешняя жизнь
2.1) Оставила комментарий
2.2) Оставила лайк записи
2.3) Оставила лайк комментарию
2.4) Оставила лайк вложенному файлу
2.5) Оставила новую запись
2.6) Загрузила фотографию в сообщество


Сбор статистики 
Количество сообщений за день
кому чаще отвечает
часто употребляемые слова
Анализ фоток, гео координаты